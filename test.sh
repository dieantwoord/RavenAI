#!/bin/bash

#This script will find the IPs with the most connections and add all of its requests together. It will also show what day and what hours the most requests in high frequency were made.

#Set variables for log file and output file
LOG_FILE="/var/log/httpd/access_log"
OUTPUT_FILE="output.txt"

#Find IPs with most connections and add all of its requests together 
echo "Finding IPs with most connections..." 
cat $LOG_FILE | awk '{print $1}' | sort | uniq -c | sort -nr > $OUTPUT_FILE 
echo "Done!" 
echo "" 
echo "The IPs with the most connections are:" 
cat $OUTPUT_FILE 
echo "" 

 #Find what day and what hours the most requests in high frequency were made  
echo "Finding what day and what hours the most requests in high frequency were made..."  
cat $LOG_FILE | awk '{print $4}' | cut -d: -f1,2 | sort | uniq -c | sort -nr > $OUTPUT_FILE  
echo "Done!"  
echo ""  
echo "The day and hour with the highest request frequency is:"  
cat $OUTPUT_FILE
