const client = require("../raven_ai");
const fs = require('fs')
const jsonfile = require('jsonfile')
const { glob } = require("glob");
const { promisify } = require("util");
const globPromise = promisify(glob);

client.on("interactionCreate", async (interaction) => {
// Slash Commands
const slashCommands = await globPromise(`${process.cwd()}/commands/*/*.js`);
const arrayOfSlashCommands = [];
slashCommands.map((value) => {
  const file = require(value);
  const splitted = value.split("/");
  const directory = splitted[splitted.length - 2];

  if (!file?.name) return;

  const properties = { directory, ...file };
  client.slashCommands.set(file.name, properties);

  if (["MESSAGE", "USER"].includes(file.type)) delete file.description;
  arrayOfSlashCommands.push(file);
});
  // Slash Command Handling
  if (interaction.isChatInputCommand()) {

    // If we have a modal, lets make sure to skip the deferal.
    if (interaction.commandName === 'advanced'){
    console.log("Modal detected, skipping...")
    } else {
      // If not, defer and wait for edits - Max 15 Minutes!
      // Send defer depending on what the user has their privacy set to
      await interaction.deferReply({ ephemeral: false }).catch(() => { });
    }
    
    // Get command info
    const cmd = client.slashCommands.get(interaction.commandName);
    if (!cmd)
      return interaction.followUp({ content: "An error has occurred " });
    // Args, Options and Subdommands handling from module.exports.
    const args = [];
    for (let option of interaction.options.data) {
      if (option.type === "SUB_COMMAND") {
        if (option.name) args.push(option.name);
        option.options?.forEach((x) => {
          if (x.value) args.push(x.value);
        });
      } else if (option.value) args.push(option.value);
    }
    // Set GUILD member
    interaction.member = interaction.guild.members.cache.get(interaction.user.id);

    // Run Command
    cmd.run(client, interaction, args);
  }


  // Context Menu Handling
  if (interaction.isContextMenuCommand()) {
    await interaction.deferReply({ ephemeral: false });
    const command = client.slashCommands.get(interaction.commandName);
    if (command) command.run(client, interaction);
  }
});